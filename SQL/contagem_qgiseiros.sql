SELECT mnp.geocodigo,
	unf.nome AS `estado`,
	mnp.nome AS "municipio",
	COUNT(1) AS "qt_qgiseiros"
FROM Censo_de_Usuarios_QGIS_tratado AS cns
JOIN municipio AS mnp ON (mnp.geocodigo = cns.geocodigo)
JOIN unidade_federacao AS unf ON ( st_contains(unf.geometry, point_on_surface(mnp.geometry)))
WHERE mnp.geocodigo like '31%'
GROUP BY mnp.geocodigo
ORDER BY "qt_qgiseiros" DESC
LIMIT 5;