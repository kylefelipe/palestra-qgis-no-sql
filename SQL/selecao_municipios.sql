SELECT row_number() OVER (
	ORDER BY "mnp"."nome") AS 'row_id',
	`mnp`."nome" AS 'nm_municipio',
	cns.*,
	point_on_surface(`mnp`.geometry) AS geometry
FROM municipio AS "mnp"
JOIN Censo_de_Usuarios_QGIS_tratado AS cns ON (cns.geocodigo = mnp.geocodigo)
WHERE mnp.geocodigo like '31%'
ORDER BY "row_id";